# Data viewer

An application that retrieves from its url an id of a uploaded data in [data-share](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-share) of a customized table or chart, and renders it. It allows users to publish refined views of dataflows outside of [data-explorer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer).

> forked from https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-webapp  
> multi-tenant (proxy, config required)  
> docker: [siscc/dotstatsuite-data-viewer](https://cloud.docker.com/u/siscc/repository/docker/siscc/dotstatsuite-data-viewer)

---
**dev:**
```
git clone https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config
cd dotstatsuite-config
yarn
yarn && yarn start:srv
```
```
git clone https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-viewer
cd dotstatsuite-data-viewer
yarn
yarn start:proxy
yarn start:srv
curl http://localhost:7000/api/healthcheck | json_pp
```

**prod:**
- check `gitlab-ci.yml` for required steps before starting the service
- check `Dockerfile` for image and/or cmd to execute to start the service
- check https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-kube-rp to see the service definition

---
**for further details, see https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-webapp**
