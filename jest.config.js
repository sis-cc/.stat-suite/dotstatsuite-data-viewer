module.exports = {
  clearMocks: true,
  collectCoverageFrom: ['!src/proxy/**', 'src/**/*.js', '!src/**/*.test.{js,jsx}', '!src/web/**'],
  coverageDirectory: 'coverage',
  testEnvironment: 'node',
  testRegex: 'src/.*\\.test\\.js$',
};
