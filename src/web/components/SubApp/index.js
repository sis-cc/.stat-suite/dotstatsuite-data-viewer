import React from 'react';
import * as R from 'ramda';
import { NoData, Loading } from '@sis-cc/dotstatsuite-visions';
import { FormattedMessage } from '../../i18n';
import Viewer from '../Viewer';

export default ({ isFetching, notFound, pendingConfirmation, isLayoutIncomptaible, data, error, ...rest }) => {
  if (isFetching) return <Loading message={<FormattedMessage id="data.fetching" />} />;

  if (notFound) return <NoData message={<FormattedMessage id="data.not.found" />} />;
  if (pendingConfirmation) return <NoData message={<FormattedMessage id="data.pending" />} />;
  if (isLayoutIncomptaible) return <NoData message={<FormattedMessage id="layout.incompatible" />} />;

  if (R.isNil(data) || R.isEmpty(data)) return <NoData message={<FormattedMessage id="data.none" />} />;

  if (error) return <NoData message={<FormattedMessage id="data.error" values={{ message: error.message }} />} />;

  return <Viewer {...rest} data={data} />;
};
