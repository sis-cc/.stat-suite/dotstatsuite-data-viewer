import React from 'react';
const R = require('ramda');
import PropTypes from 'prop-types';
import sanitizeHtml from 'sanitize-html';

const SanitizedInnerHTML = ({ html, ...rest }) => {
  if (!R.is(String, html)) {
    return html;
  }
  return <span dangerouslySetInnerHTML={{ __html: sanitizeHtml(html, window.SETTINGS?.htmlSanitization) }} {...rest} />;
};

SanitizedInnerHTML.propTypes = {
  html: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

export default SanitizedInnerHTML;
