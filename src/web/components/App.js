import React, { useState, useEffect } from 'react';
import axios from 'axios';
import * as R from 'ramda';
import { rules, rules2 } from '@sis-cc/dotstatsuite-components';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import CssBaseline from '@mui/material/CssBaseline';
import numeral from 'numeral';
import { I18nProvider } from '../i18n';
import { getLatestData, parseHeadersRange, getFrequency, getHiddenIds } from '../lib/utils';
import { SNAPSHOT, LATEST, NO_SOURCE } from '../lib/constants';
import { shareViewEvent } from '../lib/analytics';
import Helmet from './Helmet';
import SubApp from './SubApp';
import { ThemeProvider } from '../theme';
import { getSdmxRequestArgs, getUrlLocale } from '../lib/searchUrl';

const App = () => {
  const [state, setState] = useState({
    isFetching: false,
    isRtl: false,
    isLayoutIncomptaible: false,
    error: null,
    locale: 'en',
    range: null,
    type: undefined,
    terms: null,
    timeFormats: null,
    data: undefined,
    pendingConfirmation: undefined,
    notFound: undefined,
    mode: undefined,
    observationsType: undefined,
    display: undefined,
  });

  useEffect(() => {
    const fetchData = async () => {
      const params = new URLSearchParams(window.location.search);
      const shareId = params.get('chartId');

      if (!shareId) {
        setState(prev => ({ ...prev, error: new Error('no id') }));
        return;
      }

      setState(prev => ({ ...prev, isFetching: true }));

      try {
        const res = await axios.get(`${R.path(['SETTINGS', 'share', 'endpoint'])(window)}/api/charts/${shareId}`);
        const sharedData = R.path(['data', 'data'], res);

        if (R.path(['data', 'status'], res) === 'PENDING') {
          setState(prev => ({ ...prev, isFetching: false, pendingConfirmation: true }));
          return;
        }

        if (R.either(R.isNil, R.isEmpty)(sharedData)) {
          setState(prev => ({ ...prev, isFetching: false }));
          return;
        }

        const { isRtl, mode, type } = sharedData;
        const locale = R.pathOr({}, ['config', 'locale'], sharedData);
        const terms = R.pathOr({}, ['config', 'terms'], sharedData);
        const timeFormats = R.pathOr({}, ['config', 'timeFormats'], sharedData);
        const observationsType = R.pathOr(undefined, ['config', 'observationsType'], sharedData);
        const display = R.pathOr('label', ['config', 'display'], sharedData);
        const upperType = R.toUpper(type);

        const getTitle = R.path(['headerProps', 'title', 'label']);

        if (mode === SNAPSHOT) {
          shareViewEvent({
            locale,
            customLabel: `${getTitle(sharedData)} ${shareId}`,
            label: `${upperType} - ${getTitle(sharedData)} ${shareId}`,
            type: upperType,
          });

          const { range, cellsLimit } = R.propOr({}, 'config', sharedData);

          setState(prev => ({
            ...prev,
            isFetching: false,
            type,
            data: R.assocPath(['tableProps', 'cellsLimit'], cellsLimit)(sharedData),
            isRtl,
            terms,
            timeFormats,
            range,
            mode,
            observationsType,
            display,
          }));
        }

        if (mode === LATEST) {
          const urlLocale = getUrlLocale();
          if (!R.isNil(urlLocale)) {
            setState(prev => ({ ...prev, locale: urlLocale }));
          }

          const { url, headers, params } = getSdmxRequestArgs(sharedData);
          const isSDMX3 = R.pipe(R.prop('Accept'), R.includes(rules2.SDMX_3_0_JSON_DATA_FORMAT))(headers);

          const parseJson = (json, hierarchies = {}) => {
            const dataflowId = R.path(['config', 'sdmxSource', 'identifiers', 'code'], sharedData);
            const hiddenIds = getHiddenIds(json);
            const { locale, timeFormat } = R.prop('config', sharedData);
            const frequency = getFrequency(json);
            const options = { locale, timeFormat, dataflowId, frequency, hiddenIds };
            const v7json = R.pipe(rules.v8Transformer, R.prop('data'))(json, options);
            const isLayoutCompatible = rules.isSharedLayoutCompatible(v7json, sharedData);

            if (!isLayoutCompatible) {
              setState(prev => ({ ...prev, isLayoutIncomptaible: true }));
              throw Error('incompatible layout');
            }
            return getLatestData(sharedData, v7json, hierarchies);
          };

          if (url && url !== NO_SOURCE) {
            try {
              const response = await axios.get(url, { headers, params });
              const range = SDMXJS.parseDataRange(response);
              const limit = parseHeadersRange(headers);

              setState(prev => ({ ...prev, range: { ...range, limit } }));

              const json = isSDMX3 ? rules2.sdmx_3_0_DataFormatPatch(response.data) : response.data;

              const hCodelistsRefs = rules2.getHCodelistsRefsInData(json);

              let data;
              if (!R.isEmpty(hCodelistsRefs)) {
                const results = await Promise.all(
                  R.map(({ codelistId, hierarchy, ...identifiers }) => {
                    const { url, headers, params } = SDMXJS.getRequestArgs({
                      identifiers,
                      datasource: R.path(['config', 'sdmxSource', 'datasource'], sharedData),
                      type: 'hierarchicalcodelist',
                    });

                    return axios
                      .get(url, { headers, params })
                      .then(res => {
                        const parsed = R.pipe(
                          R.pathOr([], ['data', 'hierarchicalCodelists', 0, 'hierarchies']),
                          R.find(R.propEq('id', hierarchy)),
                          h => (R.isNil(h) ? [] : h.hierarchicalCodes || []),
                        )(res);
                        return { codelistId, hierarchy: parsed };
                      })
                      .catch(() => ({}));
                  }, R.values(hCodelistsRefs)),
                );

                const hierarchies = R.reduce(
                  (acc, { codelistId, hierarchy }) => (codelistId ? R.assoc(codelistId, hierarchy, acc) : acc),
                  {},
                  results,
                );

                data = parseJson(json, hierarchies);
              } else {
                data = parseJson(json);
              }

              shareViewEvent({
                locale,
                customLabel: `${getTitle(sharedData)} ${shareId}`,
                label: `${upperType} - ${getTitle(data)} ${shareId}`,
                type: upperType,
              });

              setState(prev => ({
                ...prev,
                isFetching: false,
                type,
                data,
                isRtl,
                terms,
                timeFormats,
                mode,
                observationsType,
                display,
              }));
            } catch (error) {
              setState(prev => ({ ...prev, isFetching: false, error }));
            }
          }
        }

        const delimiters = R.pathOr({}, ['SETTINGS', 'i18n', 'locales', locale, 'delimiters'], window);

        if (!R.isNil(delimiters) && !R.isEmpty(delimiters)) {
          numeral.locale(`${locale}/${locale}`);
          numeral.register('locale', `${locale}/${locale}`, { delimiters });
        }
      } catch (error) {
        if (R.path(['response', 'status'], error) === 404) {
          setState(prev => ({ ...prev, isFetching: false, notFound: true }));
        } else {
          setState(prev => ({ ...prev, isFetching: false, error }));
        }
      }
    };

    fetchData();
  }, []);

  return (
    <ThemeProvider isRtl={state.isRtl}>
      <I18nProvider localeId={state.locale} messages={window.I18N}>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Helmet lang={state.locale} isRtl={state.isRtl} />
          <CssBaseline />
          <SubApp {...state} />
        </div>
      </I18nProvider>
    </ThemeProvider>
  );
};

export default App;
