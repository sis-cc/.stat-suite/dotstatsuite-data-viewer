import React from 'react';
import PropTypes from 'prop-types';
import { IntlProvider } from 'react-intl';

const Provider = ({ localeId, messages, children }) => {
  return (
    <IntlProvider locale={localeId} key={localeId} messages={messages[localeId]}>
      {React.Children.only(children)}
    </IntlProvider>
  );
};

Provider.propTypes = {
  localeId: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired,
};

export default Provider;
