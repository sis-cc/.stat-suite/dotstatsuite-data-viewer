import ReactGA from 'react-ga';
import TagManager from 'react-gtm-module';
import * as R from 'ramda';

const isDev = process.env.NODE_ENV === 'development';
/*const getIsGTMToken = ({ tagManagerArgs }) => tagManagerArgs?.gtmId && !R.isEmpty(tagManagerArgs?.gtmId);

export const getHasToken = ({ gaToken, tagManagerArgs }) => {
  if (getIsGTMToken({ tagManagerArgs })) return true;
  if (gaToken && !R.isEmpty(gaToken)) return true;
  return false;
};*/

const getIsGTMToken = () => !R.isEmpty(R.path(['CONFIG', 'gtmToken'], window));

export const getHasToken = () => {
  if (getIsGTMToken()) return true;
  return !R.isEmpty(R.path(['CONFIG', 'gaToken'], window));
};
//---------------------------------------------------------------------------------------initialize
export const initialize = ({ tagManagerArgs = {}, dataLayer = {} }) => {
  if (!getHasToken()) return;
  const gtmToken = R.path(['CONFIG', 'gtmToken'], window);
  const gaToken = R.path(['CONFIG', 'gaToken'], window);
  if (!R.isEmpty(gtmToken)) {
    if (process.env.NODE_ENV !== 'production') {
      // eslint-disable-next-line no-console
      console.info(`google-tag-manager initialized with ${tagManagerArgs.gtmId}`);
    }
    TagManager.initialize({
      ...tagManagerArgs,
      gtmId: gtmToken,
      dataLayer: {
        site_environment: R.path(['CONFIG', 'siteEnv'], window),
        ...dataLayer,
      },
    });
    return;
  }

  if (process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line no-console
    console.info(`google-analytics initialized with ${gaToken}`);
  }
  ReactGA.initialize(gaToken, { debug: isDev });
  return;
};

export const shareViewEvent = ({ customLabel, label, type, locale }) => {
  const analytics = R.pathOr({}, ['SETTINGS', 'analytics'])(window);
  sendEvent(getHasToken(analytics))({
    dataLayer: {
      ...(analytics.dataLayer || {}),
      category: 'SHARE_VIEW',
      action: `VIEWED_SHARED_${type}`,
      label,
      event: 'load_dataflow',
      customCategory: 'navigation',
      customAction: 'viewDataflow',
      customLabel,
      nonInteraction: true,
      pageLanguage: locale,
      isGTM: getIsGTMToken(analytics),
    },
  });
};

export const sendEvent = hasToken => ({ dataLayer }) => {
  if (!hasToken) return;
  if (R.prop('isGTM', dataLayer)) {
    return TagManager.dataLayer({
      dataLayer: R.omit(['category', 'action', 'label', 'isGTM'], dataLayer),
    });
  }
  ReactGA.event(dataLayer);
};
