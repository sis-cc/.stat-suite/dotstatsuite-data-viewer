import * as R from 'ramda';
import qs from 'qs';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';

export const getUrlLocale = () =>
  R.pipe(
    searchUrl => qs.parse(searchUrl, { ignoreQueryPrefix: true, comma: true }),
    R.prop('lc'),
  )(window.location.search);

export const getSdmxRequestArgs = data => {
  const { locale, sdmxSource } = R.propOr({}, 'config', data);

  const { datasource, identifiers, dataquery, params, ...rest } = sdmxSource;

  const urlParams = R.pipe(
    searchUrl => qs.parse(searchUrl, { ignoreQueryPrefix: true, comma: true }),
    R.evolve({
      lo: R.pipe(val => Number(val), R.when(isNaN, R.always(undefined))),
      pd: R.pipe(R.split(','), R.map(R.when(R.isEmpty, R.always(undefined)))),
    }),
    R.props(['dq', 'lo', 'lc', 'pd']),
    ([dataquery, lastNObservations, locale, period]) => ({ dataquery, lastNObservations, locale, period }),
    R.reject(R.isNil),
    R.when(R.has('period'), ({ period, ...rest }) => {
      if (!R.is(Array, period)) {
        return rest;
      }
      const [startPeriod, endPeriod] = period;
      return { startPeriod, endPeriod, ...rest };
    }),
  )(window.location.search);

  return R.pipe(
    getRequestArgs,
    R.over(R.lensProp('headers'), headers => {
      const hasCustomRangeHeader = R.propOr(true, 'hasCustomRangeHeader', datasource);
      const range = R.prop('x-range', headers);
      if (hasCustomRangeHeader || R.isNil(range)) {
        return headers;
      }
      return R.pipe(R.assoc('range', range), R.dissoc('x-range'))(headers);
    }),
  )({
    datasource,
    identifiers,
    type: 'data',
    params: R.mergeRight(params, R.pick(['lastNObservations', 'startPeriod', 'endPeriod'], urlParams)),
    locale: R.propOr(locale, 'locale', urlParams),
    dataquery: R.propOr(dataquery, 'dataquery', urlParams),
    ...rest,
  });
};
