import debug from '../debug';

const error = (err, req, res, next) => {
  if (!err) return next();
  debug.error(err.stack);
  const code = err.code || 500;
  return res.status(code).json({ message: err.message, code: code, type: err.type });
};

module.exports = error;
