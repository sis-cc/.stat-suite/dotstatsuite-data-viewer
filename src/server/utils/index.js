const R = require('ramda');

export const initResources = resources => ctx =>
  R.reduce((acc, initFn) => acc.then(initFn), Promise.resolve(ctx), resources);
