const R = require('ramda');
const initHealthcheck = require('./healthcheck');

const ressources = [initHealthcheck];
const init = ctx =>
  R.reduce((acc, initFn) => acc.then(initFn), Promise.resolve(ctx), ressources);
module.exports = init;
