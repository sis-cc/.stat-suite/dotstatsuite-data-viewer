const helmet = require('helmet');
import * as R from 'ramda';
import crypto from 'crypto';
import cors from 'cors';
import compression from 'compression';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import { healthcheckConnector } from '../services/healthcheck/connector';
import errorHandler from '../middlewares/errors';
import tenant from '../middlewares/tenant';
import ssr from '../ssr';
import { HTTPError } from '../utils/errors';
import brotli from '../middlewares/brotli';

const BUILD_PATH = path.join(__dirname, '../../../build');
const PUBLIC_PATH = path.join(__dirname, '../../../public');

const checkTenant = (req, res, next) => {
  if (!req.member) return next(new HTTPError(400, 'Tenant required'));
  next();
};

const init = ctx => {
  const app = express();
  app.disable('x-powered-by');

  const {
    services: { healthcheck },
    configProvider,
    config: { isProduction },
  } = ctx;

  app.use(cors({ origin: 'http://localhost' }));
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  app.get('/robots.txt', (req, res) => res.sendFile(path.resolve(__dirname, '../robots.txt')));
  app.use(compression());
  app.use(express.static(PUBLIC_PATH));
  app.get('*.js', brotli({ buildPath: BUILD_PATH }));
  app.use(
    express.static(BUILD_PATH, {
      setHeaders: (res, path) => {
        if (path.endsWith('.js.br')) {
          res.setHeader('Content-Type', 'application/javascript');
          res.setHeader('Content-Encoding', 'br');
        }
      },
    }),
  );
  app.use(tenant(configProvider));
  app.use((req, res, next) => {
    res.locals.cspNonce = crypto.randomBytes(16).toString('hex');
    next();
  });
  if (isProduction) {
    app.use(helmet());
    app.use(async (req, res, next) => {
      const { member } = req;
      const settings = R.is(Function, configProvider.getSettings) ? await configProvider.getSettings(member) : {};
      const authorisedDomains = R.pipe(
        R.path(['app', 'authorisedDomains']),
        R.ifElse(R.is(Array), R.identity, R.always([])),
        R.without(['*', "'unsafe-eval'"]),
      )(settings);
      const scriptTags = R.pipe(
        R.path(['app', 'scriptTags']),
        R.ifElse(R.is(Array), R.identity, R.always([])),
        R.without(['*', "'unsafe-eval'"]),
        R.map(linkTag => {
          const url = new URL(linkTag);
          return `${url.protocol}//${url.host}`;
        }),
      )(settings);
      const linkTags = R.pipe(
        R.path(['app', 'linkTags']),
        R.ifElse(R.is(Array), R.identity, R.always([])),
        R.without(['*', "'unsafe-eval'"]),
        R.map(linkTag => {
          const url = new URL(linkTag);
          return `${url.protocol}//${url.host}`;
        }),
      )(settings);

      return helmet.contentSecurityPolicy({
        useDefaults: true,
        directives: {
          'upgrade-insecure-requests': null,
          'default-src': ["'self'"],
          'connect-src': ['*'], // enabling to connect to sfs, nsi, share...
          'script-src': [
            `'nonce-${res.locals.cspNonce}'`,
            "'self'",
            "'strict-dynamic'",
            "'unsafe-inline'",
            'https://www.google-analytics.com',
            'https://www.googletagmanager.com',
            ...authorisedDomains,
            ...scriptTags,
          ],
          'style-src': [
            "'self'",
            "'unsafe-inline'", // app don't work
            ...authorisedDomains,
            ...linkTags,
          ],
          'font-src': ["'self'", 'data:', 'fonts.gstatic.com'],
          'frame-src': [
            "'self'",
            req => {
              const authority = req?.member?.scope?.oidc?.authority;
              if (!authority) return '';
              return R.pipe(
                R.defaultTo([]),
                R.prepend(authority),
                R.map(alias => new URL(alias).host),
                R.join(' '),
              )(req?.member?.scope?.oidc?.authority_aliases);
            },
          ],
          'frame-ancestors': ['*', "'self'"],
          'img-src': ['*', 'data:', ...authorisedDomains],
        },
      })(req, res, next);
    });
  }
  app.use((req, res, next) => {
    res.setHeader('Permissions-Policy', 'camera=(), fullscreen=(), microphone=(), payment=()');
    next();
  });
  app.get('/api/healthcheck', healthcheckConnector(healthcheck));
  app.use(checkTenant, ssr(ctx));
  app.use(errorHandler);

  return Promise.resolve({ ...ctx, app });
};

module.exports = init;
