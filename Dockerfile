FROM node:18-alpine

ARG GIT_HASH

RUN mkdir -p /opt
WORKDIR /opt

RUN echo $GIT_HASH

COPY build /opt/build
COPY dist /opt/dist
COPY package.json package-lock.json /opt/

RUN npm install --omit=dev --legacy-peer-deps --force

ENV GIT_HASH=$GIT_HASH

EXPOSE 80
CMD yarn start:run
